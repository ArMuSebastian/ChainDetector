//
//  Exported+Module.swift
//  
//
//  Created by Artem Myshkin on 18.10.2021.
//

public enum Module {

    public typealias Core = ChainDetectorCoreModule

    public typealias ClassicDetector = ClassicChainDetector

}
