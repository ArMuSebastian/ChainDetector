//
//  CDCBoardCell.swift
//  
//
//  Created by Artem Myshkin on 02.03.2022.
//

public protocol CDCBoardCell {

    associatedtype FirstLevel: CDCElement
    associatedtype SecondLevel: CDCTile

    var first: FirstLevel? { get }
    var second: SecondLevel { get }

    init(first: FirstLevel?, second: SecondLevel)

}
