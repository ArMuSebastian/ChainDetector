//
//  CDCTileType.swift
//  
//
//  Created by Artem Myshkin on 15.08.2021.
//

public protocol CDCTileType: Hashable {

    static var hole: Self { get }

}
